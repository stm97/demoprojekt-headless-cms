
import React, {Component} from 'react';
import Head from 'next/head'
import {Table} from 'react-bootstrap';
import styles from '../../styles/Home.module.css'
import cstyles from 'react-responsive-carousel/lib/styles/carousel.min.css'
import Navigation from '../../components/navigation'
import {contentfulclient} from '../api/contentfulclient'
import { documentToHtmlString } from '@contentful/rich-text-html-renderer'
import { Carousel } from 'react-responsive-carousel';


export default class Produkt extends Component {
    constructor(props) {
        super(props);
        this.state = {
          beschreibung: "",
          produkt: [],
          produktbilder: [],
          spez: []
        }
    }

    async componentDidMount() {
      
      var path = window.location.pathname.split('/').reverse()
        
        const res = await contentfulclient.getEntry(path[0])

        const beschreibung = documentToHtmlString(res.fields.beschreibung)

        const bilder = res.fields.produktbilder

        const spez = res.fields.spezifikationen
        
        this.setState({
          produkt: res.fields,
          beschreibung: beschreibung,
          produktbilder: bilder,
          spez: spez,
        })

      }
    

  render (bilder){

    const pictures = this.state.produktbilder.map((x, key) =>
      <div key={key}>
        <img src={x.fields.file.url} className={styles.produktbild}/>
      </div>
    );

    const spez = Object.keys(this.state.spez).map((key, x) => 
      <tr key={x}>
        <td>{key}</td>
        <td>{this.state.spez[key]}</td>
      </tr>
    );

    return(
      <div className={styles.container}>
        <div className={styles.wrap}>
          <Head>
            <title>Headless Demo</title>
            <meta name="description" content="Beispielapp Headless CMS" />
            <link
              rel="stylesheet"
              href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous"
            />
          </Head>

          <main className={styles.main}>
            <Navigation headerid="6skhLSB3qC1otljsGOFZwR"/>
            <h1>{this.state.produkt.produkttitel}</h1>
            <Carousel>{pictures}</Carousel>
            <div dangerouslySetInnerHTML={{__html: this.state.beschreibung}}></div>
            <h4 className={styles.preis}>Preis: {this.state.produkt.preis} CHF</h4>
            <h3>Spezifikationen</h3>
            <Table className={styles.tabel}>
              <tbody>
                {spez}
              </tbody>
            </Table>
          </main>

          <footer className={styles.footer}>
            
          </footer>
        </div>
        

      </div>

      
    )
  }
}