import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Home from '../components/home'
import Navigation from '../components/navigation';
import {contentfulclient} from './api/contentfulclient'


export async function getStaticProps() {
  
  const res = await contentfulclient.getEntry("715CdGEF7JaqH0MSAJT1Wv")

  return {
    props: {
      references: res.fields.references,
    }
  }

}


export default function Index() {

  return (
      <div className={styles.container}>
        <div className={styles.wrap}>
          <Head>
            <title>Headless Demo</title>
            <meta name="description" content="Beispielapp Headless CMS" />
            <link
              rel="stylesheet"
              href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous"
            />
          </Head>

          <main className={styles.main}>
            <Navigation headerid="6skhLSB3qC1otljsGOFZwR"/>
            <Home homepageid="715CdGEF7JaqH0MSAJT1Wv" />
          </main>

          <footer className={styles.footer}>
            
          </footer>
        </div>
        
      </div>
  )
}
