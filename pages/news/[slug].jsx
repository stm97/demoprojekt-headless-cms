
import React, {Component} from 'react';
import Head from 'next/head'
import styles from '../../styles/Home.module.css'
import Navigation from '../../components/navigation'
import {contentfulclient} from '../api/contentfulclient'
import Teaserprodukt from '../../components/teaserprodukt'
import Newsteaser from '../../components/newsteaser'


export default class News extends Component {
    constructor(props) {
        super(props);
        this.state = {
          news: [],
          titel: "",
          headline: "",
          produktid: [],
          bannerbild: "",
          beschreibungstexte: [],
        }
    }

    async componentDidMount() {
      
      var path = window.location.pathname.split('/').reverse()
        
      const res = await (await contentfulclient.getEntry(path[0])).fields
      const verl = []
  
      verl.push(res.verlinkung.sys.id);
      
      this.setState({
        news: res,
        titel: res.titel,
        headline: res.headline,
        beschreibungstexte: res.beschreibungstexte,
        produktid: verl,
        bannerbild: res.bannerBild.fields.file.url,
      })

      }
    
  render (bilder){

    const produkt = this.state.produktid.map((x) => <Teaserprodukt produktid={x} />);
    const captions = this.state.beschreibungstexte.map((x) => <Newsteaser teaserid={x.sys.id} />)

    return(
      <div className={styles.container}>
        <div className={styles.wrap}>
          <Head>
            <title>Headless Demo</title>
            <meta name="description" content="Beispielapp Headless CMS" />
            <link
              rel="stylesheet"
              href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous"
            />
          </Head>

          <main className={styles.main}>
            <Navigation headerid="6skhLSB3qC1otljsGOFZwR"/>
            <h1>{this.state.titel}</h1>
            <h3>{this.state.headline}</h3>
              <a href={/produkt/ + this.state.produktid}>
                <img src={this.state.bannerbild} className={styles.produktbild}></img>
              </a>
              {captions}
            <h4>Produkt</h4>
              {produkt}
            
          </main>

          <footer className={styles.footer}>
            
          </footer>
        </div>
        
      </div>
      
    )
  }
}