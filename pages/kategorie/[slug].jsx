import {contentfulclient} from '../api/contentfulclient'
import React, {Component} from 'react';
import Head from 'next/head'
import styles from '../../styles/Home.module.css'
import Navigation from '../../components/navigation'
import Teaserprodukt from '../../components/teaserprodukt'


export default class Produkt extends Component {
    constructor(props) {
        super(props);
        this.state = {
          produkte: [],
        }
    }

    async componentDidMount() {
      
      var path = window.location.pathname.split('/').reverse()
        
        const res = await contentfulclient.getEntries({
          content_type: 'produkt',
          'fields.kategorie.sys.id': path[0]
        })
        
        this.setState({
          produkte: res.items
        })
      }
    

  render (res){

    const list = this.state.produkte.map((x) => 
      <Teaserprodukt produktid={x.sys.id} />
    );

    return(
      <div className={styles.container}>
        <div className={styles.wrap}>
          <Head>
            <title>Headless Demo</title>
            <meta name="description" content="Beispielapp Headless CMS" />
            <link
              rel="stylesheet"
              href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
              crossorigin="anonymous"
            />
          </Head>

          <main className={styles.main}>
            <Navigation headerid="6skhLSB3qC1otljsGOFZwR"/>
            {list}
          </main>

          <footer className={styles.footer}>
            
          </footer>
        </div>

      </div>

    )
  }
}