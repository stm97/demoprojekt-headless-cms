import React, {Component} from 'react';
import {Nav} from 'react-bootstrap';
import styles from '../styles/Home.module.css'
import {contentfulclient} from '../pages/api/contentfulclient'


export default class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
          elemente: [],
        }
    }

    async componentDidMount() {

        const res = await contentfulclient.getEntry(this.props.headerid)
        this.setState({
          elemente: res.fields.navigation,
        })

      }
    

  render (){

    const navigation = this.state.elemente.map((link) => 
    
    <Nav.Item key={link.sys.id}>
        <Nav.Link href={"/kategorie/" + link.sys.id}>{link.fields.titel}</Nav.Link>
    </Nav.Item>

    );
    
    return(
    <div className={styles.nav}>
        <Nav className="justify-content-center" >
            <Nav.Item>
                <Nav.Link href={"/"}>Home</Nav.Link>
            </Nav.Item>
            {navigation}
        </Nav>
    </div>
    
      
    )
  }
}