import React, {Component} from 'react';
import {contentfulclient} from '../pages/api/contentfulclient'


export default class Produkt extends Component {
    constructor(props) {
        super(props);
        this.state = {
          produkte: [],
        }
    }

    async componentDidMount() {
        
        const res = await contentfulclient.getEntries({
          content_type: 'produkt',
          'fields.kategorie.sys.id': this.props.kategorieid
        })
        
        this.setState({
          produkte: res.items
        })

      }
    

  render (res){

    const list = this.state.produkte.map((x) => 
      <Teaser produktid={x.sys.id}/>
    );

    return(
        <div>
          {list}
        </div>
      
    )
  }
}