import React, {Component} from 'react';
import styles from '../styles/Home.module.css'
import {contentfulclient} from '../pages/api/contentfulclient'
import { documentToHtmlString } from '@contentful/rich-text-html-renderer'


export default class Newsteaser extends Component {
    constructor(props) {
        super(props);
        this.state = {
          titel: "",
          beschreibung: "",
          bild: "",
          bildgroesse: 0,
          lr: "Rechts"
        }
    }

    async componentDidMount() {

      const res = await contentfulclient.getEntry(this.props.teaserid)
      const beschreibung = documentToHtmlString(res.fields.beschreibungs)
        
        this.setState({
          titel: res.fields.titel,
          beschreibung: beschreibung,
          bild: res.fields.bild.fields.file.url,
          bildgroesse: res.fields.bildgroesse,
          lr: res.fields.linksRechts
        })

      }
    
  render (){

    if(this.state.lr == "Rechts"){
        return <div className={styles.smallteaserrigth}>
        <div>
        <h2>{this.state.titel}</h2>
        <div dangerouslySetInnerHTML={{__html: this.state.beschreibung}}></div>
        </div>
        
        <img src={this.state.bild}width={this.state.bildgroesse + "%"}></img>
      </div>
      }
      if(this.state.lr == "Links"){
        return <div className={styles.smallteaserleft}>
        <div>
        <h2>{this.state.titel}</h2>
        <div dangerouslySetInnerHTML={{__html: this.state.beschreibung}}></div>
        </div>
        
        <img src={this.state.bild}width={this.state.bildgroesse + "%"}></img>
      </div>
      }

    return(
      <div>

      </div>
        
    )
  }
}