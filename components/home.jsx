import React, {Component} from 'react';
import styles from '../styles/Home.module.css'
import {contentfulclient} from '../pages/api/contentfulclient'
import Banner from './banner'
import Teaserprodukt from './teaserprodukt';
import Teasernews from './teasernews'


export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
          references: [],
        }
    }

    async componentDidMount() {
        
        const res = await (await contentfulclient.getEntry(this.props.homepageid)).fields
        
        this.setState({
          references: res.references
        })

      }
    

  render (){

    const site = this.state.references.map((ref) => 
      
          {
            if(Object.keys(ref.fields)[1] == "banners") {
              return <Banner bannerid={ref.fields.banners[0].sys.id} />
            }
            
            if(Object.keys(ref.fields)[1] == "news") {
              return <div className={styles.homediv}>
                        <h2>News</h2>
                        <div className={styles.homenews}>
                        {ref.fields.news.map((x) => <Teasernews newsid={x.sys.id} /> )}
                        </div>
                      </div>     
              
            }

            if(Object.keys(ref.fields)[1] == "produkt") {
              return <div className={styles.homediv}>
                        <h2>Beispielprodukte</h2>
                        <div className={styles.homeprodukt}>
                        {ref.fields.produkt.map((x) => <Teaserprodukt produktid={x.sys.id} /> )}
                        </div>
                      </div>     
              
            }  
          }
        );

    return(
        <div>
          {site}
        </div>
      
    )
  }
}