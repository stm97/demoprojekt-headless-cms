const contentful = require('contentful')
import React, {Component} from 'react';
import {Carousel} from 'react-bootstrap';
import {contentfulclient} from '../pages/api/contentfulclient'


export default class Banner extends Component {
    constructor(props) {
        super(props);
        this.state = {
          titel: "",
          kurzbeschreibung: "",
          anzeigebild: "",
          referenzprodukt: "",
          contentType: "",
        }
    }

    async componentDidMount() {

      const res = await contentfulclient.getEntry(this.props.bannerid)
      this.setState({
        titel: res.fields.titel,
        kurzbeschreibung: res.fields.kurzbeschreibung,
        anzeigebild: res.fields.anzeigebild.fields.file.url,
        referenzprodukt: res.fields.referenzprodukt.sys.id,
        contentType: res.sys.contentType.sys.id
      })
  
    }
    

  render (){
    
    return(
        <Carousel variant="dark" controls="false">
          <Carousel.Item>
            <a href={/produkt/ + this.state.referenzprodukt}>
            <img src={this.state.anzeigebild} width="100%"/>
            <Carousel.Caption>
              <h2>{this.state.titel}</h2>
              <p>{this.state.kurzbeschreibung}</p>
            </Carousel.Caption>
            </a>
          </Carousel.Item>
        </Carousel>
      
    )
  }
}