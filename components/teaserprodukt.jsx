import React, {Component} from 'react';
import {Card} from 'react-bootstrap';
import styles from '../styles/Home.module.css'
import {contentfulclient} from '../pages/api/contentfulclient'


export default class Teaserprodukt extends Component {
    constructor(props) {
        super(props);
        this.state = {
          produktid: "",
          produktbild: "",
          produkttitel: "",
          kurzbeschreibung: "",
        }
    }

    async componentDidMount() {
      
        const res = await contentfulclient.getEntry(this.props.produktid)
        this.setState({
          produktid: res.sys.id,
          produktbild: res.fields.produktbilder[0].fields.file.url,
          produkttitel: res.fields.produkttitel,
          kurzbeschreibung: res.fields.kurzbeschreibung,
        })

      }
    

  render (){
    
    return(
    
      <Card style={{ width: '20rem' }} className={styles.card} >
      <a href={/produkt/ + this.state.produktid}>
      <Card.Img variant="top" src={this.state.produktbild}  className={styles.produktbild}/>
      <Card.Body>
        <Card.Title>{this.state.produkttitel}</Card.Title>
        <Card.Text>{this.state.kurzbeschreibung}</Card.Text>
      </Card.Body>
      </a>
    </Card>
    )
  }
}