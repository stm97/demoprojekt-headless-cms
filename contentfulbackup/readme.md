# Importieren der Backup Datei

Neuen Contentful Account anlegen.

## Installation Contentful cli im Terminal
`npm install -g contentful-cli`

`contentful login`

Anweisungen Folgen bis Login erfolgreich

## Import Backup

- Wurde bei der Erstellung des Accounts ein Beispielprojekt erstellt den Space unter Settings > General settings löschen und leeren Space erstellen

- Unter Settings > General settings die SpaceId kopieren

- Im Terminal im contentfulbackup Ordner folgenden Befehl ausführen.

`contentful import --space-id hierspaceideinfügen --content-file ./contentful-backup.json`

- Nach erfolgreichem import sollten das Contentmodel sowie der Content auf Contentful zu sehen sein.

- In der Datei /demoprojekt-headless-cms/pages/api/contentfulclient.js die SpaceId in Zeile 4 ersetzen

- In der selben Datei accessToken in Zeile 6 ersetzen. Token ist zu finden unter Settings > API keys > Headless Demo 1 > Content Delivery API - access token
